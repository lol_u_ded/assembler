#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "game_2048.h"


#define BOARD_SIZE 4

// seed=100 in=input.txt out=output.txt

bool eof = false;

/*  read_direction  */
/*
  get the current user input related to directions

  * Parameters:
    FILE *stream            : stream where the input is read from (stdin or input-file)

  * Return:
    enum Directions         : current read direction, presented as enum
*/
Directions read_direction(FILE *stream)
{
    char c;

    printf("Enter w, a, s, d for direciton:\n");
    while((c=getc(stream))=='\n');
    if (c == EOF)
    {
        eof=true;
        return DIR_UNDEFINED;
    }
    printf("char: %c\n",c);

    // check directions here and return the correct one or DIR_UNDEFINED
    if (c == 'w')
    {
        return DIR_UP;
    }
    else if (c == 'a')
    {
        return DIR_LEFT;
    }
    else if (c == 's')
    {
        return DIR_DOWN;
    }
    else if (c == 'd')
    {
        return DIR_RIGHT;
    }
    else
    {
        return DIR_UNDEFINED;
    }
}


/*  save_current_state  */
/*
  save the current state of the array to a file for testability

  * Parameters:
    Board *board             : pointer to struct Board, which contains the array of Cells and the size of the sides
    FILE *stream             : stream where the output is written to
    const unsigend int round : number of current round
*/
void save_current_state(Board *board, FILE *stream, const unsigned int round)
{
    int row;
    int col;
    int size;
    Cell **c;

    c = *(board->cells);
    size = board->size;

    fprintf(stream,"round:%d\n", round);
    for(row = 0; row < size; row++)
    {
        for(col = 0; col < size; col++)
        {
            fprintf(stream,"%d , ",c[row][col].value);
        }
        fprintf(stream,"\n");
    }
}

void save_dir(FILE *stream, Directions dir)
{

    if (dir == DIR_UP)
    {
        fprintf(stream,"w");
    }
    else if (dir == DIR_LEFT)
    {
        fprintf(stream,"a");
    }
    else if (dir == DIR_DOWN)
    {
        fprintf(stream,"s");
    }
    else if (dir == DIR_RIGHT)
    {
        fprintf(stream,"d");
    }
    else
    {
        fprintf(stream,"\n ERROR \n");
    }

}


/*  main  */
int main (int argc, char *argv[])
{
    Error_code err = EXIT_OK;
    int i;
    char *curr;

    // Standard values for input, output, seed
    FILE *in = in =stdin;                           // for Debug: fopen("input.txt", "r");
    FILE *out = fopen("output.txt", "w");
    FILE *path = fopen("path.txt", "w");            // DEBUG-FILE to recreate input path
    int seed = 1;


    // getting new values that will simply override std-Values
    if(argc >1)
    {
        for(i=1; i<argc; i++)
        {
            curr = strtok(argv[i], "=");
            if(strcmp(curr, "in") == 0)
            {
                curr = strtok(NULL, "in");
                in = fopen(curr, "r");
            }
            else if(strcmp(curr, "out") == 0)
            {
                curr = strtok(NULL, "=");
                out = fopen(curr, "w");
            }
            else if(strcmp(curr, "seed") == 0)
            {
                curr = strtok(NULL, "=");
                seed = atoi(curr);
            }
            else
            {
                err = EXIT_ARGS;
            }
        }
    }

    // Creating gameBoard & initialising it
    Board *board = create_board(BOARD_SIZE, seed, &err);
    Board_state gameState = STATE_ONGOING;
    Directions d;
    int round = 0;

    // GameLoop
    // * -> Read in value -> moveDirection -> print to console -> save in Outputfile -> nextRound ->*
    while(gameState == STATE_ONGOING)
    {
        d = read_direction(in);
        save_dir(path,d);       // save path into a file to recreate any situation (also its a saveFile :3 ) 
        gameState = move_direction(board, d, &err);
        print_board(board,false);
        save_current_state(board, out, round);
        round++;
        if(eof)
        {
            gameState = STATE_FINISH;
        }
    }

    // Closing Files after game & return Error Code
    fclose(in);
    fclose(out);

    return err;
}






