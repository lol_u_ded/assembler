#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "game_2048.h"

bool moved = false;         // will be used to

////////////////////////////////////////////////////////////
//     Helper functions (used only inside this c-file)    //
////////////////////////////////////////////////////////////

// checks if pos(x/y) can move to any direction
bool movePossible(int row, int col, Board *board)
{

    int size;
    int val;
    int curVal;
    int curRow;
    int curCol;
    Cell **c;

    c = *(board->cells);
    size = board->size;
    val = c[row][col].value;

    // Checking all directions of currentField

    // ( row+1 | col )
    curRow = row+1;
    if( curRow < size )
    {
        curVal = c[curRow][col].value;
        if(curVal == 0 || curVal == val)        // Check if field is empty or could be merged
        {
            return true;
        }
    }

    // ( row-1 | col )
    curRow = row-1;
    if( curRow >= 0)
    {
        curVal = c[curRow][col].value;
        if(curVal == 0 || curVal == val)        // Check if field is empty or could be merged
        {
            return true;
        }
    }

    // ( row | col+1 )
    curCol = col+1;
    if( curCol < size)
    {
        curVal = c[row][curCol].value;
        if(curVal == 0 || curVal == val)        // Check if field is empty or could be merged
        {
            return true;
        }
    }

    // ( row | col-1 )
    curCol = col-1;
    if( curCol >= 0)
    {
        curVal = c[row][curCol].value;
        if(curVal == 0 || curVal == val)        // Check if field is empty or could be merged
        {
            return true;
        }
    }

// If there is no move possible return false

    return false;
}

// adds a new value either 4 or 2 to a random position.
// returns false if it fails to do so
bool addNewVal(Board *board)
{
    int row = rand()%board->size;       // random row position
    int col = rand()%board->size;       // random col position
    Cell** c;
    c = *(board->cells);

    int number = rand()%2;              // decide if 2 or 4 will be inserted
    if(number == 0)
    {
        number = 2;
    }
    else
    {
        number = 4;
    }
    if(c[row][col].value == 0)
    {
        c[row][col].value = number;
        return false;                   // return false if inserting was not possible
    }
    return true;
}

// takes a position from(x/y) and merges value over to(x/y)
void mergeVal(Board *board, int rowFrom, int colFrom, int rowTo, int colTo)
{
    Cell** c;
    c = *(board->cells);
    c[rowTo][colTo].value += c[rowFrom][colFrom].value;
    c[rowFrom][colFrom].value = 0;
    moved = true;
}

// takes a position from(x/y) and moves value over to(x/y)
void moveVal(Board *board, int rowFrom, int colFrom, int rowTo, int colTo)
{
    Cell** c;
    c = *(board->cells);
    c[rowTo][colTo].value = c[rowFrom][colFrom].value;
    c[rowFrom][colFrom].value = 0;
    moved = true;
}

// moves and merges up
void up( Board *board)
{

    Cell** c;
    c = *(board->cells);
    int size = board->size;

    int row;
    int col;
    int i;

    for(i = 0; i < 2; i++)              // 2 nd check is needed for cornercase
    {
        // Move at first the numbers
        for(row = size-1; row > 0; row--)
        {
            for(col= 0; col < size; col++)
            {
                // if the cell is empty move value over
                if(c[row-1][col].value == 0 && c[row][col].value != 0)
                {
                    moveVal(board,row, col, row-1, col);
                }
            }
        }
    }

    // Then merge the numbers
    for(row = 0; row < size-1; row++)
    {
        for(col = 0; col < size; col++)
        {
            if(c[row][col].value != 0)
            {
                if(c[row][col].value == c[row+1][col].value)
                {
                    mergeVal(board,row+1, col, row, col);
                }
            }
        }
    }

    //another move to close the gaps
    for(row = size-1; row > 0; row--)
    {
        for(col= 0; col < size; col++)
        {
            if(c[row-1][col].value == 0 && c[row][col].value != 0)
            {
                moveVal(board,row, col, row-1, col);
            }
        }
    }
    // print_board(board,false);
}

// moves and merges down
void down( Board *board)
{
    Cell** c;
    c = *(board->cells);
    int size = board->size;

    int row;
    int col;
    int i;

    for(i = 0; i < 2; i++)      // 2 nd check is needed for cornercase
    {
        // Move at first the numbers
        for(row = 0; row < size-1; row++)
        {
            for(col= 0; col < size; col++)
            {
                // if the cell is empty move value over
                if(c[row+1][col].value == 0 && c[row][col].value != 0)
                {

                    moveVal(board,row, col, row+1, col);
                }
            }
        }
    }

    // Then merge the numbers
    for(row = size-1; row  > 0; row--)
    {
        for(col = 0; col < size; col++)
        {
            if(c[row][col].value != 0 )
            {
                if(c[row][col].value == c[row-1][col].value)
                {
                    mergeVal(board,row-1, col, row, col);
                }
            }
        }
    }

    //another move to close the gaps
    for(row = 0; row < size-1; row++)
    {
        for(col= 0; col < size; col++)
        {
            if(c[row+1][col].value == 0 && c[row][col].value != 0)
            {
                moveVal(board,row, col, row+1, col);
            }
        }
    }
}

// moves and merges to the right side
void right( Board *board)
{
    Cell** c;
    c = *(board->cells);
    int size = board->size;

    int row;
    int col;
    int i;

    for(i = 0; i < 2; i++)              // 2 nd check is needed for cornercase
    {
        // Move at first the numbers
        for(row = 0; row < size; row++)
        {
            for(col= 0; col < size-1; col++)
            {
                // if the cell is empty move value over
                if(c[row][col+1].value == 0 && c[row][col].value != 0)
                {
                    moveVal(board,row, col, row, col+1);
                }
            }
        }
    }

    // Merge the values if possible
    for(row = 0; row < size; row++)
    {
        for(col = size-1; col > 0; col--)
        {
            if(c[row][col].value != 0)
            {
                if(c[row][col].value == c[row][col-1].value)
                {
                    mergeVal(board, row, col-1,row, col);
                }
            }
        }
    }

    // another move to close the gaps
    for(row = 0; row < size; row++)
    {
        for(col= 0; col < size-1; col++)
        {
            if(c[row][col+1].value == 0 && c[row][col].value != 0)
            {
                moveVal(board,row, col, row, col+1);
            }
        }
    }
}

// moves and merges to the left side
void left(Board *board)
{
    Cell** c;
    c = *(board->cells);
    int size = board->size;

    int row;
    int col;
    int i;

    for(i = 0; i < 2; i++)      // 2 nd check is needed for cornercase
    {
        // Move at first the numbers
        for(row = 0; row < size; row++)
        {
            for(col= size-1; col > 0; col--)
            {
                // if the cell is empty move value over
                if(c[row][col-1].value == 0 && c[row][col].value != 0)
                {
                    moveVal(board,row, col, row, col-1);
                }
            }
        }
    }

    // Then merge the numbers
    for(row = 0; row < size; row++)
    {
        for(col = 0; col < size-1; col++)
        {
            if(c[row][col].value != 0)
            {
                if(c[row][col].value == c[row][col+1].value)
                {
                    mergeVal(board,row, col+1, row, col);
                }
            }
        }
    }

    //another move to close the gaps
    for(row = 0; row < size; row++)
    {
        for(col= size-1; col > 0; col--)
        {
            if(c[row][col-1].value == 0 && c[row][col].value != 0)
            {
                moveVal(board,row, col, row, col-1);
            }
        }
    }
}

// This method will just forward to the move methods up,down, right, left
void mov(Board *board, Directions dir)
{

    // get directions
    if(dir == DIR_UP)
    {
        up(board);
    }
    else if(dir == DIR_DOWN)
    {
        down(board);
    }
    else if(dir == DIR_LEFT)
    {
        left(board);
    }
    else // dir == DIR_RIGHT
    {
        right(board);
    }
}


// Initial board setUp
void begin(Board *board)
{
    addNewVal(board);
    addNewVal(board);
    print_board(board,true);
}

//////////////////////////////////////////////////////////////
//  END  Helper functions (used only inside this c-file)    //
//////////////////////////////////////////////////////////////



/*  move_direction  */
/*
  move the board values to the given direction

  * Parameters:
    Board *board            : pointer to struct Board, which contains the array of Cells and the size of the sides
    Directions dir          : enum, that descripes the direction of the move (up, down, left, right); in this case the direction to move
    ErrorCode *err          : Typedef and Macros used to check for invalid behavior. (call-by-ref)

  * Return value:
    Board_state        : enum, that describes the current state of the board
*/
Board_state move_direction (Board *board, Directions dir, Error_code *err)
{
    Board_state state = check_board_state(board);
    bool pending = true;
    if(state == STATE_NO_MORE_MOVES || state == STATE_FINISH)
    {
        return state;
    }

    // move & merge numbers
    if(dir != DIR_UNDEFINED)
    {
        mov(board,dir);

        if(moved)       // only add a new value if there was a movement and/or  a merge
        {
            while(pending)      //
            {
                pending = addNewVal(board);
            }
        }
        moved = false;
    }
    return STATE_ONGOING;
}




/*  create_board  */
/*
  function to create a new board to play on, incl. memory allocation

  * Parameters:
    const unsigned int size : size of the board (NxN), used for width and height
    const unsigned int seed : seed value used for srand to initialize the random number generation
    Error_code *err         : Typedef of enum used to check for invalid behavior. (call-by-ref)

  * Return value:
    Board *board            : pointer to struct Board, which contains the array of Cells and the size of the sides
*/
Board* create_board(const unsigned int size, const unsigned int seed, Error_code *err)
{
    Cell **rptr;
    Board *board;
    int k;

    /* next we allocate room for the pointers to the rows */
    rptr = (Cell **) malloc(size * sizeof(Cell *));

    for (k = 0; k < size; k++)
    {
        /* we now allocate the memory for the array */
        rptr[k] = (Cell *) malloc(size * sizeof(Cell));

    }

    /* next we allocate room for the pointers to the Board */
    board = (Board *) malloc(sizeof(Board));

    // Error cases
    if (rptr == NULL||board == NULL )
    {
        *err = EXIT_OUT_OF_MEM;
    }
    else
    {

        board ->cells = (Cell***) malloc(sizeof(Cell**));
        board->cells[0] = rptr;
        board -> size = size;

        // init cell values & positions
        for(int a= 0; a < size; a++)
        {
            for(int b= 0; b < size; b++)
            {
                rptr[a][b].pos.x = a;
                rptr[a][b].pos.y = b;
                rptr[a][b].value = 0;
            }
        }
    }

    srand(seed);        // set the ranom seed
    begin(board);       // init the board

    return board;
}

/*  remove_board  */
/*
  remove board, which is passed via parameters and free memory

  * Parameters:
    Board *board            : pointer to struct Board, which contains the array of Cells and the size of the sides
*/
void remove_board(Board *board)
{
    free(board);
}


/*  check_board_state  */
/*
  check the state of the board: if finish condition reached, if empty cells available or if moves possible

  * Parameters:
    Board *board       : pointer to struct Board, which contains the array of Cells and the size of the sides

  * Return value:
    Board_state        : enum, that describes the current state of the board
*/
Board_state check_board_state(Board *board)
{
    int row;
    int col;
    int size;
    Cell **c;
    bool onGoing;

    c = *(board->cells);
    size = board->size;
    onGoing = false;


    for(row = 0; row < size; row++)
    {
        for(col = 0; col < size; col++)
        {
            // STATE_FINISH
            if(c[row][col].value == 2048)       // wining condition
            {
                printf("STATE_FINISH\n");
                return STATE_FINISH;
            }
            // STATE_NO_MORE_MOVES
            if(movePossible(row, col, board))
            {
                onGoing = true;
                goto end;               // one legal move was found so we can break out of the loop
            }
        }
    }
end:
    // STATE_ONGOING
    if(onGoing)
    {
        printf("STATE_ONGOING\n");
        return STATE_ONGOING;
    }
    else                           // ongoing is false which translates there was no cell found that can move
    {
        printf("STATE_NO_MORE_MOVES\n");
        return STATE_NO_MORE_MOVES;
    }

    return STATE_ONGOING;
}

/*  print_board  */
/*
  print the current board state to the terminal

  * Parameters:
    Board *board            : pointer to struct Board, which contains the array of Cells and the size of the sides
    bool with_clear         : if true, no clear is done for the terminal, else the terminal is cleared and overwritten
*/
void print_board(Board *board, bool with_clear)
{
    int row;
    int col;
    int size;
    Cell **c;

    c = *(board->cells);
    size = board->size;

    printf("---------------------------\n");
    for(row = 0; row < size; row++)
    {
        for(col = 0; col < size; col++)
        {
            printf("|    %d ",c[row][col].value);
        }
        printf("|\n");
        printf("---------------------------\n");
    }

}


