# PURPOSE:   This program converts an input file into hamming code and stores
#	     the result into the output file.
#
# PROCESSING: 1) Open the input file
#             2) Open the output file
#             3) While we're not at the end of the input file
#               a) read part of the file into our read buffer
#               b) go through each byte of the read buffer
#                  - convert lower nibble and store in write buffer
#                  - convert upper nibble and store in write buffer
#               c) write the write buffer to the output file

.section .data
    # The encoding matrix G
    encode_matrix: .byte 0b1101, 0b1011, 0b1000, 0b0111, 0b0100, 0b0010, 0b0001

#######CONSTANTS########

	# System call numbers
	.equ SYS_OPEN, 2
	.equ SYS_READ, 0
	.equ SYS_WRITE, 1
	.equ SYS_CLOSE, 3
	.equ SYS_EXIT, 60

	# Return Values
	.equ RET_OK, 0
	.equ RET_ERROR, 1

	# Options for open   (look at /usr/include/asm/fcntl.h for
	#                    various values.  You can combine them
	#                    by adding them)
	.equ O_RDONLY, 0                  # Open file options - read-only
	.equ O_CREAT_WRONLY_TRUNC, 03101  # Open file options - these options are:
	                                  # CREAT - create file if it doesn't exist
	                                  # WRONLY - we will only write to this file
	                                  # TRUNC - destroy current file contents, if any exist

	.equ O_PERMS, 0666                # Read & Write permissions for everyone

	# End-of-file result status
	.equ END_OF_FILE, 0  # This is the return value of read() which
	                     # means we've hit the end of the file

#######BUFFERS#########

.section .bss
	# This is where the data is loaded into from the data file
	.equ READ_BUFFER_SIZE, 250
	.lcomm READ_BUFFER_DATA, READ_BUFFER_SIZE
	# This is where the data is written to after conversion
	# and from here it is also written to the output file
	.lcomm WRITE_BUFFER_DATA, READ_BUFFER_SIZE*2


#######PROGRAM CODE###

	.section .text

	# STACK POSITIONS
	.equ ST_SIZE_RESERVE, 16 # Space for local variables
	# Note: Offsets are RBP-based, which is set immediately at program start
	.equ ST_FD_IN, -16       # Local variable for input file descriptor
	.equ ST_FD_OUT, -8       # Local variable for output file descriptor
	.equ ST_ARGC, 0          # Number of arguments
	.equ ST_ARGV_0, 8        # Name of program
	.equ ST_ARGV_1, 16       # Input file name
	.equ ST_ARGV_2, 24       # Output file name

	.globl _start
_start:
	###INITIALIZE PROGRAM###


# ----------------------------------------------------------------------------------------------------

#####FUNCTION encode
#
#PURPOSE:   This function actually implements the conversion to hamming 8,4
#
#INPUT:     The first parameter (DIL) is the 8-bit value that we want to encode.
#           Since we encode into the hamming 8,4 format we are only interested in
#           the lower nibble (a nibble being half a byte or 4-bit) of the input byte.
#           Our input format looks like:
#     bit    7       0
#           +----+----+
#           |xxxx|vvvv|
#           +----+----+
#           x = dont care
#           v = value to encode
#
#OUTPUT:
#           Return value: This function returns one byte containing
#           the hamming encoded lower nibble of the input value
#           using "encode_matrix" for calculation
#
#VARIABLES:
#
encode:
