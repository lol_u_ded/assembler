# PURPOSE:   This program converts an input file into hamming code and stores
#	     the result into the output file.
#
# PROCESSING: 1) Open the input file
#             2) Open the output file
#             3) While we're not at the end of the input file
#               a) read part of the file into our read buffer
#               b) go through each byte of the read buffer
#                  - convert lower nibble and store in write buffer
#                  - convert upper nibble and store in write buffer
#               c) write the write buffer to the output file

.section .data
    # The encoding matrix G
    encode_matrix: .byte 0b1101, 0b1011, 0b1000, 0b0111, 0b0100, 0b0010, 0b0001

#######CONSTANTS########

	# System call numbers
	.equ SYS_OPEN, 2
	.equ SYS_READ, 0
	.equ SYS_WRITE, 1
	.equ SYS_CLOSE, 3
	.equ SYS_EXIT, 60

	# Return Values
	.equ RET_OK, 0
	.equ RET_ERROR, 1

	# Options for open   (look at /usr/include/asm/fcntl.h for
	#                    various values.  You can combine them
	#                    by adding them)
	.equ O_RDONLY, 0                        # Open file options - read-only
	.equ O_CREAT_WRONLY_TRUNC, 03101        # Open file options - these options are:
                                            # CREAT - create file if it doesn't exist
                                            # WRONLY - we will only write to this file
                                            # TRUNC - destroy current file contents, if any exist

	.equ O_PERMS, 0666                      # Read & Write permissions for everyone

	# End-of-file result status
	.equ END_OF_FILE, 0                     # eof

#######BUFFERS#########

.section .bss
	# This is where the data is loaded into from the data file
	.equ READ_BUFFER_SIZE, 250
	.lcomm READ_BUFFER_DATA, READ_BUFFER_SIZE
	# This is where the data is written to after conversion
	# and from here it is also written to the output file
	.lcomm WRITE_BUFFER_DATA, READ_BUFFER_SIZE*2


#######PROGRAM CODE###

	.section .text

	# STACK POSITIONS
	.equ ST_SIZE_RESERVE, 16                # Space for local variables
	.equ ST_FD_IN, -16                      # Local variable for input file descriptor
	.equ ST_FD_OUT, -8                      # Local variable for output file descriptor
	.equ ST_ARGC, 0                         # Number of arguments
	.equ ST_ARGV_0, 8                       # Name of program
	.equ ST_ARGV_1, 16                      # Input file name
	.equ ST_ARGV_2, 24                      # Output file name

	.globl _start
_start:
    ### IMPORTANT ####
    # The read in from a file & write out to a file is based from the lecture
    # slides "convert to upper and the example toupper.s"

	###INITIALIZE PROGRAM###
	movq %rsp, %rbp                         # Create new stack frame
	subq $ST_SIZE_RESERVE, %rsp             # Allocate space for our file descriptors on the stack
	###CHECK PARAMETER COUNT###
	cmpq $3, ST_ARGC(%rbp)
	je open_files
	movq $1, %rdi                           # Our return value for parameter problems
	movq $SYS_EXIT, %rax
	syscall

open_files:
open_fd_in:
	###OPEN INPUT FILE###
	movq ST_ARGV_1(%rbp), %rdi              # Input filename into %rdi
	movq $O_RDONLY, %rsi                    # Read-only flag
	movq $O_PERMS, %rdx                     # Set permissions
	movq $SYS_OPEN, %rax                    # Specify "open"
	syscall 	                            # Call Linux
	cmpq $0, %rax                           # Check success
	jl exit                                 # In case of error simply terminate

store_fd_in:
	movq  %rax, ST_FD_IN(%rbp)              # Save the returned file descriptor

open_fd_out:
	###OPEN OUTPUT FILE###
	movq ST_ARGV_2(%rbp), %rdi              # Output filename into %rdi
	movq $O_CREAT_WRONLY_TRUNC, %rsi        # Flags for writing to the file
	movq $O_PERMS, %rdx                     # Permission set for new file
	movq $SYS_OPEN, %rax                    # Open the file
	syscall                                 # Call Linux
	cmpq $0, %rax                           # Check success
	jl close_input                          # In case of error close input file

store_fd_out:
	movq %rax, ST_FD_OUT(%rbp)              # Store the file descriptor

	###BEGIN MAIN LOOP###
read_loop_begin:

	###READ IN A BLOCK FROM THE INPUT FILE###
	movq ST_FD_IN(%rbp), %rdi               # Get the input file descriptor
	movq $READ_BUFFER_DATA, %rsi            # The location to read into
	movq $READ_BUFFER_SIZE, %rdx            # The size of the buffer
	movq $SYS_READ, %rax
	syscall                                 # Size of buffer read is returned in %eax

	###EXIT IF WE'VE REACHED THE END###
	cmpq $END_OF_FILE, %rax                 # Check for end of file marker
	je end_loop                             # If found (or error), go to the end
	jl close_output                         # On error just terminate

continue_read_loop:
	###ENCODE THE BLOCK###
	movq $READ_BUFFER_DATA, %rdi            # Location of the read buffer
	movq $WRITE_BUFFER_DATA, %rcx           # Location of the write buffer
	movq %rax, %rsi                         # Size of the buffer
	pushq $-1                               # Dummy value for stack alignment
	pushq %rax                              # Store number of bytes read for write check
	call encode	                            # begin encoding

write_out_begin:
	###WRITE THE BLOCK OUT TO THE OUTPUT FILE###
	movq ST_FD_OUT(%rbp), %rdi              # File to use
	movq $WRITE_BUFFER_DATA, %rsi           # Location of  buffer
	movq %rax, %rdx                         # Size of buffer
	movq $SYS_WRITE, %rax
	syscall
	###CHECK WRITE SUCCESS###
	popq %rbx                               # Retrieve number of bytes read
	addq $8, %rsp                           # Remove stack alignment space
	cmpq %rax, %r13                         # Compare number read to written
	jne close_output                        # If not equal, terminate program
	###CONTINUE THE LOOP###
	jmp read_loop_begin

end_loop:                                   # No special error handling done.
close_output:
	###CLOSE THE FILES###
	movq ST_FD_OUT(%rbp), %rdi
	movq $SYS_CLOSE, %rax
	syscall

close_input:
	movq ST_FD_IN(%rbp), %rdi
	movq $SYS_CLOSE, %rax
	syscall

exit:
	###EXIT###
	movq $0, %rdi                           # Standard return value for all cases
	movq $SYS_EXIT, %rax
	syscall

# ----------------------------------------------------------------------------------------------------

#####FUNCTION encode
#
#PURPOSE:   This function actually implements the conversion to hamming 8,4
#
#INPUT:     The first parameter (DIL) is the 8-bit value that we want to encode.
#           Since we encode into the hamming 8,4 format we are only interested in
#           the lower nibble (a nibble being half a byte or 4-bit) of the input byte.
#           Our input format looks like:
#     bit    7       0
#           +----+----+
#           |xxxx|vvvv|
#           +----+----+
#           x = dont care
#           v = value to encode
#
#OUTPUT:
#           Return value: This function returns one byte containing
#           the hamming encoded lower nibble of the input value
#           using "encode_matrix" for calculation
#
#VARIABLES:
#      %rax     = buffer
#      %rdx     = read buffer size
#      %rcx     = output buffer
#      %rbp
#      %rsp
#      %r8b     = lower nibble
#      %r9b     = upper nibble
#      %r10b    = working register a
#      %r11b    = working register b  , will contain upper nibble
#      %r12b    = result from loop
#      %r13b    = counter  i
#      %r14b    = counter for loop lower/upper


encode:
    pushq %rbp                              # Prepare stack
	movq  %rsp, %rbp
	pushq %rbx                              # Save RBX

	###SET UP VARIABLES###
	movq %rdi, %rax                         # move into rax
	movq %rsi, %rbx
	movq $0, %rdi
	mov $0, %r8                             # lower nipple l
	mov $0, %r9                             # upper nipple u
	mov $0, %r13                            # counter i
	mov $0, %r14                            # counter for loop lower
    mov $0, %r15                            # counter for loop upper
    movq $encode_matrix, %r15               # adress encode_matrix

	# If a buffer with zero length was given us, just leave
	cmpq $0, %rbx
	je end_encode_loop

###encoding upper and lower with two for loops###
matrix_loop:
    movq $0, %r12                           # clear output register
    movq $0, %r11                           # clear b
    movb (%rax, %rdi, 1), %r8b              # save 8bit into r8b
    movb %r8b, %r9b                         # copy into r9b aswell
    and $0x0F, %r8b                         # mask lower
    and $0xF0, %r9b                         # mask upper
    shrb $4, %r9b                           # shift upper to lower

# FOR-LOOP_lower
    movq $0, %r14                           # Initialize loop counter
for_lower:
    movq $0, %r10                           # reset a
    movq $0, %r11                           # reset b
    cmp $7,%r14                             # At the end of the loop?
    je for_end_lower                        # If not, go to end
    movb %r8b, %r10b                        # copy lower nibble to a
    and (%r15, %r14,1), %r10                # current matrix line && lower nibble
    popcnt %r10d, %r10d                     # popcount
    and $1, %r10b                           # get LSB
    shl $1, %r12b                           # shift previous bit one left
    or %r10b, %r12b                         # write LSB to output reg
    inc %r14b                               # Increment loop counter
    jmp for_lower                           # Go to loop begin
for_end_lower:

# FOR-LOOP_upper
    movq $0, %r14                           # reset loop counter
    movq $0, %r10                           # reset working register
for_upper:
    movq $0, %r10                           # reset a
    cmp $7,%r14                             # At the end of the loop?
    je for_end_upper                        # If not, go to end
    movb %r9b, %r10b                         # copy upper nibble to a
    and (%r15,%r14,1), %r10                 # current matrix line && upper nibble
    popcnt %r10d, %r10d                     # popcount to b
    and $1, %r10b                           # get LSB
    shl $1, %r11b                           # shift previous bit one left
    or %r10b, %r11b                         # write LSB to output reg
    inc %r14b                               # Increment loop counter
    jmp for_upper                           # Go to loop begin
for_end_upper:
	movq $0, %r8                            # reset l
	movq $0, %r9                            # reset u
	shlq $8, %r11                           # shift left 8 bit
	orq %r11, %r12                          # add upper
    movq %r12, (%rcx)                       # add 16 bit to output
    addq $2, %rcx                           # shift by 2 byte
    addq $2, %r13                           # i+2
    inc %rdi                                # increase rdi
    cmpq %rdi, %rbx                         # check if another loop is needed
    jne matrix_loop

end_encode_loop:
    movq %r13, %rax                         # return the value
	popq %rbx
	movq %rbp, %rsp
	popq %rbp
	ret


