#include <stdlib.h>
#include <time.h>

#include "game-io.h"

// auxilary method to print out avaliable suitcases
void printSuitcases(bool array[])
{
   int loop;
    for(loop = 0; loop < 26; loop++)
    {
        if(array[loop]==false)
        {
            printf("[%d]", loop+1);
        }

    }
    printf("\n");
}

// auxilary method to count the digits of an number
int countDigits(int num)
{
    int digits = 0;
    while(num != 0 )
    {
        num /= 10;
        digits++;
    }
    return digits;
}


int main (int argc, const char* argv[])
{
    // the suitcases and if they were chosen
    long suitcases[] = {1, 100, 500, 1000, 2500, 5000, 7500, 10000, 20000, 30000, 40000, 50000, 75000, 100000, 500000, 1000000, 2500000, 5000000, 7500000, 10000000, 20000000, 30000000, 40000000, 50000000, 75000000, 100000000};
    bool chosenCases[] = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};

    int remainingValue = 341841601;         // all values of the suitcases summed up

    // the round values
    const int eliminate[] ={-1,6,5,4,3,2,1,1,1,1};      // value at zero is a dummy value, we start at round1 --> arrayposition 1
    const int  MAX_ROUND = 10;
    const int  NUM_OF_SUITCASES = 26;
    int currRound = 0;
    int remainigSuitcases = 26;

    // variables  for own suitcases and for the last one
    int mySuitcasePos = -1;
    int mySuitcase = -1;
    int lastSuitcase = -1;

    // Randomize Suitcases
    // oriented on randomizer found at https://bits.mdminhazulhaque.io/c/shuffle-items-of-array-in-c.html
    if(argc == 2 && strcmp(argv[1],"-r") == 0)
    {
        printf(suffeling_suitcase_contents);
        srand(time(NULL));
        int i;
        for(i = NUM_OF_SUITCASES-1; i > 0; i--)
        {
            int j = rand() % (NUM_OF_SUITCASES-1);
            int temp = suitcases[j];
            suitcases[j] = suitcases[i];
            suitcases[i] = temp;
        }
    }
    //Errorcases Arguments
    else if(argc > 2)
    {
        printf(arguments_too_many);
		return EXIT_FAILURE;
    }
    else if(argv[1] != NULL )
    {
        printf(argument_invalid);
		return EXIT_FAILURE;
    }

    // initial print ofthe suitcasetable
    printSuitcases(chosenCases);

    // Round zero: Choosing an suitcase
    printf(prompt_pick_prize);
    unsigned int usrInput;
    bool token = askPlayerForNumber(&usrInput);

    // False input error checks
    while(usrInput < 1 || usrInput > NUM_OF_SUITCASES|| !token)
    {
        if(usrInput < 1 || usrInput > NUM_OF_SUITCASES)     // number out of range
        {
            printf(input_out_of_range);
        }
        else                                                // any other wrong input
        {
            printf(input_invalid);
        }
        token = askPlayerForNumber(&usrInput);              // ask again for an input if there was a wrong input
    }

    // assign the chosen suitcase and its position for later usage
    mySuitcase = usrInput;
    mySuitcasePos = usrInput-1;
    chosenCases[mySuitcasePos] = true;
    printf(confirm_prize_suitcase, mySuitcase);

    // update values and and start game
    remainingValue -= suitcases[mySuitcasePos];
    currRound++;


    // game loop
    while(currRound < MAX_ROUND)
    {
        printf(intro_round, currRound, eliminate[currRound]);
        printSuitcases(chosenCases);
        int eliminalCounter = 0;

        // eliminate cases according to the array eliminate
        while(eliminalCounter < eliminate[currRound])
        {
            // get suitcases to eliminate
            printf(prompt_pick_to_eliminate);
            unsigned int usrInput;
            askPlayerForNumber(&usrInput);

            // False input error checks
            while(usrInput < 1 || usrInput > NUM_OF_SUITCASES|| chosenCases[usrInput-1])    // checks if input is out of range or already taken.
            {
                if(chosenCases[usrInput-1])                                 // already taken
                {
                    printf(suitcase_not_availalbe);
                }
                else if (usrInput < 1 || usrInput > NUM_OF_SUITCASES)       // out of range
                {
                    printf(input_out_of_range);
                }
                else                                                        // anything else
                {
                    printf(input_invalid);
                }

                askPlayerForNumber(&usrInput);                              // ask again for valid input
            }

            //eliminate
            chosenCases[usrInput-1] = true;                                 // mark the chosen one as taken
            long int decimalDigits = suitcases[usrInput-1] % 100;;
            printf(confirm_pick_to_eliminate,usrInput, (suitcases[usrInput-1] - decimalDigits) / 100, decimalDigits);

            // update Values & Suitcases
            remainingValue -= suitcases[usrInput-1];
            remainigSuitcases --;

            // if there are just two cases left (own one and the other) save its number for later useage
            if(remainigSuitcases == 2)
            {
                lastSuitcase = usrInput;
            }
            eliminalCounter++;
        }

        // calculate Bank Offer
        long expected = ((remainingValue / remainigSuitcases)* currRound )/ 10;     // initial calculation done like described in assignment
        int bankOffer;
        long int commaBank;
        int digits = countDigits(expected);

        // round up/down calculations
        if(digits > 3)
        {
            long work = expected;           // working register
            int digicnt = digits;           // counter
            while(digicnt > 3)              // get the last three digits
            {
                work /= 10;
                digicnt--;
            }
            int lastDigit = work%10;        // extract the last digit
            if(lastDigit > 0 )              // if last digit is greater zero it should be rounded up
            {
                work = work +10 - lastDigit;
            }
            int mult = 0;
            while(mult < digits-3)          // reassign the correct numbers of zeros
            {
                work *= 10;
                mult++;
            }
            bankOffer = work;               // calculate bank offer in euro
        }
        else                                // if the expected value is less than 100 cents
        {
            bankOffer = expected;           // calculate bank offer in euro

        }

        // make an offer to the player
        long int bankComma = bankOffer % 100;
        printf(intro_bank_offer, (bankOffer-bankComma)/100,bankComma);


        // Ask player if the game should continue or accept the bank offer
        printf(prompt_bank_offer);
        int yesNo = askPlayerYesNo();
        while(yesNo < 0 || yesNo > 1)       // Retry?
        {
            yesNo = askPlayerYesNo();
        }
        if(yesNo == 0)                      // Yes
        {
            printf(confirm_bank_offer_gameover_early, bankOffer);
            return EXIT_SUCCESS;
        }

        currRound++;
    }
    // last round
    printf(intro_switch_suitcase, mySuitcase,lastSuitcase);
    printf(prompt_switch_suitcase);
    int yesNo = askPlayerYesNo();
        while(yesNo < 0 || yesNo > 1)       // Retry?
        {
            yesNo = askPlayerYesNo();
        }
        if(yesNo == 0) // Yes
        {
            printf(confirm_switch_suitcase, mySuitcase, lastSuitcase);
            int temp = mySuitcase;
            mySuitcase = lastSuitcase;
            mySuitcasePos = mySuitcase--;
            lastSuitcase = temp;
        }
        printf(game_over_full_game, mySuitcase, suitcases[mySuitcasePos]);
    // get started here
    return EXIT_SUCCESS;
}



