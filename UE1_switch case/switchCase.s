.section .data
input1:       .asciz "The dog ATE my Homework!"  # -> 19
input2:       .asciz ""	# -> 0
input3:       .asciz "#!$"	# -> 0
input4:       .asciz "The dog"  # -> 6

.section .text
# ---------------------------------
# "Inverts" the case of a string: lowercase is changed to uppercase, uppercase to
# lowercase, and everything else stays the same. The conversion is in-place.
# It returns the number of characters that were inverted
# Example: "The Dog" -> "tHE dOG" returns 6
# Example: "" -> "" returns 0
# Example: "#!$" -> "#!$" returns 0
# ---------------------------------

# Overview used registers
# %rax = i (counter conversions)
# %rcx = k (current character counter)
# %rdx = input sentence (will be converted in place)
# %bl  = curr (current character)

.global _start
_start:
	movq $0,%rax
	movq $0,%rcx            # set register to 0
	movq $input1, %rdx      # move input1 into #rdx
#	movq $input2, %rdx      # move input2 into #rdx
#	movq $input3, %rdx      # move input3 into #rdx
#   movq $input4, %rdx      # move input1 into #rdx
    movb (%rdx,%rcx,1), %bl # get next character

loop:

    cmp $0, %bl             # check if NUL
    je end                  # jump to end

    cmp $65, %bl            # if curr < ascii65 A end
    jl next                 # go to next character

    cmp $122, %bl           # if curr > ascii122 z end
    jg next                 # go to next character

    cmp $90, %bl            # if curr < ascii90 A-Z cast to upper case
    jle lower               # jump to lower

    cmp $97, %bl            # if curr > ascii97 a-z cast to lower case
    jge upper               # jump to upper

next:
    movb %bl, (%rdx,%rcx,1) # write back character
    inc %rcx                # k++
    movb (%rdx,%rcx,1), %bl # get next character
    jmp loop                # jump back to loop

upper:
    sub $32, %bl            # convert to upper character
    inc %rax                # i++
    jg next                 # go to next character

lower:
    add $32, %bl            # convert to upper character
    inc %rax                # i++
    jg next                 # go to next character

end:
	# Terminate program
    movq %rax,%rdi
    movq $60,%rax
    syscall
