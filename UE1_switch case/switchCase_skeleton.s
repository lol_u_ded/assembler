.section .data
input1:       .asciz "The dog ATE my Homework!" # -> 19
input2:       .asciz ""	# -> 0
input3:       .asciz "#!$"	# -> 0

.section .text
# ---------------------------------
# "Inverts" the case of a string: lowercase is changed to uppercase, uppercase to
# lowercase, and everything else stays the same. The conversion is in-place.
# It returns the number of characters that were inverted
# Example: "The Dog" -> "tHE dOG" returns 6
# Example: "" -> "" returns 0
# Example: "#!$" -> "#!$" returns 0
# ---------------------------------

.global _start
_start:
	movq $0,%rax
	movq $input1,...
#	movq $input2,...
#	movq $input3,...

	...

end:
	# Terminate program
    movq %rax,%rdi
    movq $60,%rax
    syscall
